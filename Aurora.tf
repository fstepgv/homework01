#Unlike other RDS resources that support replication, with Amazon Aurora you do not designate a primary and subsequent replicas. 
#Instead, you simply add RDS Instances and Aurora manages the replication.

module "aurora" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  name  = "postgres"
  engine             = "aurora-postgresql"
  engine_version     = "11.12"
  vpc_id             = module.vpc.vpc_id
  allowed_security_groups = [aws_security_group.ecs.id]
  subnets = module.vpc.database_subnets
  instances = [
    {
      instance_class          = var.rds_instance_type
      identifier              = "dg-aurora-instance-1"
      availability_zone       = "us-east-1a"
    },
{
      instance_class          = var.rds_instance_type
      identifier              = "dg-aurora-instance-2"
      availability_zone       = "us-east-1b"
    },
    {
      instance_class          = var.rds_instance_type
      identifier              = "dg-aurora-instance-3"
      availability_zone       = "us-east-1c"
    }
  ]
}
