module "testing_vpn" {
  source  = "DNXLabs/client-vpn/aws"
  version = "0.4.2"
  cidr = "172.31.0.0/16"
  name = "test"
  vpc_id                        = module.vpc.vpc_id
  subnet_ids = module.vpc.public_subnets
  split_tunnel = true
}

resource "aws_security_group" "testing_vpn" {
  description = "Allows inbound VPN connection"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "Inbound VPN connection"
    from_port = 443
    protocol = "UDP"
    to_port = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}