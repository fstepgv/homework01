#SG creation allowing incoming traffic from anywhere on port 80. 

resource "aws_security_group" "alb" { 
  name   = "alb-sg"
  vpc_id = module.vpc.vpc_id


  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

} 


# create the ALB
resource "aws_alb" "alb" {
  load_balancer_type = "application"
  name = "alb"
  subnets = module.vpc.public_subnets
  security_groups = [aws_security_group.alb.id]
}

# point redirected traffic to the app. Enabling health check. 
resource "aws_alb_target_group" "target-group" {
  name = "target-group"
  port = 80
  protocol = "HTTP"
  vpc_id = module.vpc.vpc_id
  target_type = "ip"
  health_check {
    path     = "/misc/ping"
    timeout = 20
  }
   stickiness {
    type = "lb_cookie"
    cookie_name = "my-app-cookie"
    enabled = true
  }
}

# direct traffic through the ALB
resource "aws_alb_listener" "alb-listener" {
  load_balancer_arn = aws_alb.alb.arn
  port = 80
  protocol = "HTTP"
  default_action {
    target_group_arn = aws_alb_target_group.target-group.arn
    type = "forward"
  }
}