# Terraform AWS Infrastructure Deployment

This Terraform configuration automates the deployment of a set of AWS components as shown in the diagram. 

It includes the creation of a VPC with Internet Gateway , 3 private subnets , 3 public subnets with 3 NAT Gateways and 3 database subnets.There is no database subnets on diagram so if needed  I can move databases to private subnet, however it is not recommended config as I know. 

It also creates an Application Load Balancer, an Elastic File System, an Aurora PostgreSQL database with 2 Read Replicas , an ECS cluster with 3 runnings tasks on AWS Fargate  and an AWS Client VPN endpoint.

Configuration files from this repo:  

1. **VPC.tf**  - creates VPC with required AZs and subnets. Uses VPC module.

2. **ALB.tf** - creates load balancer which forwards traffic to ECS fargate containers.

3. **EFS.tf**  - creates  EFS. EFS mapped as volume to container.

4. **Aurora.tf** - creates Aurora PostgreSQL database with 1 Primary node and 2 Read replicas.

5. **main.tf** -  Creates ECS cluster. ECS tasks run pgadmin4 containers, so we can connect to Aurora and test. 

6. **VPN.tf** - Creates AWS Client VPN Endpoint. I have used module  found on the web. 





**Prerequisites**

Before deploying the infrastructure using Terraform, please ensure that the following prerequisites are met:

* You have an AWS account set up with the required permissions to create resources.
* You have Terraform installed on your machine.
* You have AWS credentials configured on your machine.


**Deployment Procedure**

To deploy the infrastructure, follow these steps:

* Clone this repository to your local machine.
* In the root directory of the repository, run ***terraform init*** to initialize Terraform.
* Run ***terraform apply*** and enter "yes" when prompted to confirm the deployment of the infrastructure.
* Wait for the deployment to complete. This may take up to 15 minutes.

**Outputs**

The following output variables are available after the infrastructure is deployed:

**alb_dns_name** - The DNS name of the Application Load Balancer.

**aurora_endpoint** - The endpoint of the Aurora PostgreSQL database.

**aurora_password**- The password for the Aurora PostgreSQL database.

**aurora_username** - The username for the Aurora PostgreSQL database.

Output example:

*alb_dns_name = "alb-2004770106.us-east-1.elb.amazonaws.com"*

*aurora_endpoint = "postgres.cluster-cgzhbu2io03f.us-east-1.rds.amazonaws.com"*

*aurora_password = sensitive*

*aurora_username = sensitive*

Outputs give all required data for  accessing the database via ALB through pgadmin.



**Testing**

To test the infrastructure that you just deployed, follow these steps:

* Get the output variables from Terraform by running ***terraform output***.  For sensitive data like aurora_password  run "***terraform output -json aurora_password***"
* Open the Application Load Balancer DNS name  , that you got from an output in a web browser. You should see a web page for one of the pgadmin containers.
* Login to the pgadmin web page using thelogin and password we set in  variables.tf ( login: user@example.com   Pass: 1234  )
* Connect to the PostgreSQL database using the output variables for the Aurora endpoint, username, and password.

This test covers most components of infrastructure on the diagram. 


**Tear Down**

When you are finished testing the infrastructure, it's important to tear it down to avoid incurring unnecessary costs. To tear down the infrastructure, follow these steps:

* In the root directory of the repository, run ***terraform destroy*** and enter "yes" when prompted to confirm the deletion of the infrastructure.
* Wait for Terraform to delete all resources.It can take up to 15 minutes. 
* Verify that all resources have been destroyed in the AWS console.






