# create a security group to access the ECS application. Traffic allowed from ALB and EFS.

resource "aws_security_group" "ecs" {
  name = "ecs-sg"
  description = "control access to the ECS containers."
  vpc_id = module.vpc.vpc_id

  ingress {
    from_port = 80
    protocol = "TCP"
    to_port = 80
    security_groups = [aws_security_group.alb.id]
  
  }

  ingress {
    from_port = 2049
    protocol = "TCP"
    to_port = 2049
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  
}


# create the ECS cluster
resource "aws_ecs_cluster" "ecs-cluster" {
  name = "ecs-cluster"

 setting {
    name  = "containerInsights"
    value = "enabled"
  }
 
}

# create and define the container task.  Mapping EFS volume. 
resource "aws_ecs_task_definition" "ecs-task" {
  family = "pgadmin_new"
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"
  cpu = 256
  memory = 1024
  
  container_definitions = jsonencode([
    {
      name            = "pgadmin-container"
      image           = "dpage/pgadmin4:latest"
      "mountPoints": [
          {
              "containerPath": "/tmp",
              "sourceVolume": "efs-html"
          }
      ],
       log_configuration = {
        log_driver = "awslogs"
        options = {
          "awslogs-group"         = "111"
          "awslogs-region"        = "us-east-1"
          "awslogs-stream-prefix" = "pgadmin-container"
        }
      },


      portMappings    = [
        {
          containerPort = 80
          hostPort      = 80
          protocol      = "tcp"
        },
      ]
      essential       = true
      environment     = [
        {
          name  = "PGADMIN_DEFAULT_EMAIL"
          value = var.user
        },
         {
          name  = "PGADMIN_DEFAULT_PASSWORD"
          value = var.pass
        }
        
      ]
    }
  ])
   volume {
    name      = "efs-html"
    efs_volume_configuration {
      file_system_id = aws_efs_file_system.efs.id
      root_directory = "/"
    }
  }
}


# Create ECS service with 3 replicas.  It should usee all 3 private subnets.
resource "aws_ecs_service" "pgadmin-service" {
  name = "pgadmin-app-service"
  cluster = aws_ecs_cluster.ecs-cluster.id
  task_definition = aws_ecs_task_definition.ecs-task.arn
  desired_count = 3
  launch_type = "FARGATE"
  platform_version = "1.4.0"
  



  network_configuration {
    security_groups = [aws_security_group.ecs.id]
    subnets = module.vpc.private_subnets
    assign_public_ip = false
  }


  

  load_balancer {
    container_name = "pgadmin-container"
    container_port = 80
    target_group_arn = aws_alb_target_group.target-group.id
  }

  depends_on = [
    aws_alb_listener.alb-listener
  ]
}

