#EFS creation

resource "aws_efs_file_system" "efs" {
  creation_token = "efs"

  tags = {
    Name = "EFS"
  }
}


resource "aws_efs_mount_target" "efs_mount_targets" {
  count           = length(module.vpc.private_subnets)
  file_system_id  = aws_efs_file_system.efs.id
  subnet_id       = module.vpc.private_subnets[count.index]
  security_groups = [aws_security_group.ecs.id]
} 