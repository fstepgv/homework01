
variable "region" {
  description = "Region where services will be running"
  default = "us-east-1"  
}

variable "postgres_db_port" {
  description = "Port exposed by Aurora"
  default = 5432
}

variable "rds_instance_type" {
  description = "Instance type for the RDS database"
  default = "db.t3.medium"
}

variable "user" {
  description = "Login for pgadmin"
  default = "user@example.com"
}

variable "pass" {
  description = "Password for pgadmin"
  default = "1234"
}

variable "vpc_cidr" {
  description = "CIDR block for VPC"
  default = "10.0.0.0/16"
}


