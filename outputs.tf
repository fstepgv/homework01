

 output "aurora_password" {
  value = module.aurora.cluster_master_password
  sensitive   = true
}


 output "aurora_username" {
  value = module.aurora.cluster_master_username
  sensitive   = true
  
}

 output "aurora_endpoint" {
  value = module.aurora.cluster_endpoint
  
}

output "alb_dns_name" {
  value = aws_alb.alb.dns_name
}



