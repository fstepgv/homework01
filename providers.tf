# Provider block to specify AWS region
provider "aws" {
  region = var.region
}
